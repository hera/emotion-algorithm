% Close any open figures
close all
% Clear all variables.
clear

% Make a figure.
f = figure(1);
% Set the key press function to call the function.
set(f,'keypressfcn',@setacctime)

% Declare a global variable to hold the set of 3D Cartesian accelerations
% being applied to the keyboard-controlled Phantom input, along with each
% acceleration's start and end time.  Each column of this variable
% represents one such acceleration pulse.  The first three elements are the
% x, y, and z accelerations in millimeters per second.  The fourth and fifth
% elements are the start and end times for this acceleration pulse in
% seconds.  We initialize with a zero force that is active from 0 to
% infinity so that the variable always has at least one column.
global keyacctime amag adur
keyacctime = [0 0 inf]'; % mm/s^2, s, s
amag = 30; % mm/s^2
adur = 0.5; % s

% Define axis limits.
axlim = 250; % mm

% Plot a dot and its projected shadows.
pos = getPos();
%h = plot3(pos(1),pos(2),pos(3),'r.','markersize',50);
s= subplot(1,2,1);
hold on
hxy = plot(pos(1),'.','markersize',50,'color',.5*[1 1 1]);
hold off

% Format the plot.
axis equal square
axis(axlim * [-.5 .5 -.5 .5])
box on
grid on
ylabel('Emotional level')
% remove xtick labels
set(gca, 'xticklabel', {[]});
% Start the timer.
tic
tHistory = 0;
posHistory = [0];
subplot(1,2,2)
%f2 = figure, % opens a new figure window
testTimePlot = plot(0, 0);
ylim([-120 120])
axis equal square

grid on
xlabel('Time (s)');
ylabel('Emotion Level');

% Display the global variable over and over.
while true
    %disp(keyacctime)
    %disp(getPos())
    pos = getPos();
    tHistory(end+1,1) = toc;
    posHistory(end+1,:) = pos';
    % Update plot values for hxy/ grey dot  
    hxy.XData = 0;
    hxy.YData = pos(1);
    % Update time plot
    testTimePlot.XData = tHistory;
    testTimePlot.YData = posHistory;
    pause(0.1)
end

%% Plot data history vs time
