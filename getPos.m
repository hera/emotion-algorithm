function position = getPos()

global keyacctime

persistent tlast pos vel k dampcoeff dampingforce

k = 0.05; %Define spring force constant.
dampcoeff = 0.50; %Define spring damping constant coefficient.
maxPosValue = 100;
minPosValue = -100;
starting_pos = [0]'; %[0 0 0]';%[140 0 28]'; % In millimeters.

% Initialize the last call time, position, and velocity with this time,
% starting position, and zeros, respectively, if this is the first call. 
if isempty(tlast)
    tlast = toc;
    pos = starting_pos;
    vel = [0]';
end

% Log the current time.
tnow = toc;

% Start over if the timer has been reset since the last call to this function.
if (tlast > tnow)
    tlast = tnow;
    pos = starting_pos;
    vel = [0]';
end

% Count the number of acceleration pulses in the global variable.
[~,nacc] = size(keyacctime);

% Go through the pulses, starting with the last one.
for i = nacc:-1:1
    if (tnow > keyacctime(3,i)) %(tnow > keyacctime(5,i))
        % This pulse is over.  Delete it from the global variable.
        keyacctime(:,i) = [];
    elseif isnan(keyacctime(1,i))
        % The user has asked us to stop the motion.
        vel = [0]';
        % Since we handled the request, delete it from the global variable.
        keyacctime(:,i) = [];
        % Check if there are other acceleration pulses in the queue.
        if (i > 2)
            % There are, so delete them.
            for j = (i-1):-1:2
                keyacctime(:,j) = [];
            end
            % Break out of the loop.
            break
        end
    elseif (tnow > keyacctime(2,i))
        % This pulse is active.  Integrate it over the time elapsed since
        % the more recent of the acceleration start time and the last time
        % this function was called to calculate the new velocity.  This is
        % done cumulatively across all of the acceleration pulses.
        dampingforce = dampcoeff * vel;
        vel = vel + (tnow - max([keyacctime(2,i) tlast])) * (keyacctime(1,i)+(-k*(pos - starting_pos)) - dampingforce);
    end
end

% Integrate the new velocity over the elapsed time to calculate the new
% position.
pos = pos + (tnow - tlast) * vel;

%Correct for threshold limits.
if (pos > maxPosValue)
    pos = maxPosValue;
elseif (pos < minPosValue)
    pos = minPosValue;
end
disp(pos)

% Set the output variable.
position = pos;

% Store this time as the last time this function was called.
tlast = tnow;


return